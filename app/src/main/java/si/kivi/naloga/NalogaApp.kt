package si.kivi.naloga

import android.app.Application
import android.content.Context
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class NalogaApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@NalogaApp)
            modules(listOf(appModule))
        }
    }
    fun getContext(): Context? {
        return this.getApplicationContext()
    }

}
