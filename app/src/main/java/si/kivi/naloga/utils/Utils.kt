package si.kivi.naloga.utils

import android.content.Context
import android.util.DisplayMetrics
import android.content.res.Resources;

class Utils {
    companion object {
        fun getHeight() :Int{
            return Resources.getSystem().getDisplayMetrics().heightPixels;
        }
        fun getWidth(): Int{
            return Resources.getSystem().getDisplayMetrics().widthPixels;
        }

    }
}