package si.kivi.naloga.utils

import okhttp3.*
import si.kivi.naloga.BuildConfig


/**
 * This will help us to test our networking code while a particular API is not implemented
 * yet on Backend side.
 */
class BackendMocker : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if (BuildConfig.DEBUG) {
            val uri = chain.request().url().uri().toString()
            val responseString = when {
                uri.endsWith("avtomobili") -> generateAvtomobili()
                uri.endsWith("motornakolesa") -> generateMotornaKolesa()
                else -> ""
            }

            return Response.Builder().code(200)
                .protocol(Protocol.HTTP_2)
                .message(responseString)
                .body(
                    ResponseBody.create(
                        MediaType.parse("application/json"),
                        responseString.toByteArray()))
                .addHeader("content-type", "application/json")
                .request(chain.request())
                .build()

        } else {
            //just to be on safe side.
            throw IllegalAccessError("MockInterceptor is only meant for Testing Purposes and " +
                    "bound to be used only with DEBUG mode")
        }
    }

}
fun generateAvtomobili(): String{

    val seznam = arrayListOf<String>( """  { "znamka": "Mazda", "model": "CX-5" },""",
    """  { "znamka": "Volkswagen", "model": "Golf" },""",
    """  { "znamka": "Renault", "model": "Clio" },""",
    """  { "znamka": "Audi", "model": "R8" },""",
    """  { "znamka": "Škoda", "model": "Octavia" },""",
    """  { "znamka": "Ford", "model": "Focus" },""")

    var json1 = """{"avtomobili": ["""
    var json2 ="""]}"""
    seznam.shuffle()

    return json1+seznam.joinToString(" ")+json2

}
fun generateMotornaKolesa(): String{

    val seznam = arrayListOf<String>(
        """  { "znamka": "ZeroFX", "model": "The Zero FXS" },""",
        """  { "znamka": "Tomos", "model": "CTX" },""",
        """  { "znamka": "Suzuki", "model": "GSX-R" },""",
        """  { "znamka": "Harley Davidson", "model": "Deluxe" },""")

    var json1 = """{"motorji": ["""
    var json2 ="""]}"""
    seznam.shuffle()

    return json1+seznam.joinToString(" ")+json2

}