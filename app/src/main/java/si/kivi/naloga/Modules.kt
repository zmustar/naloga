package si.kivi.naloga

import org.koin.dsl.module
import si.kivi.naloga.data.model.Vozilo

val appModule= module{
    factory { Vozilo(get(),get()) }
}
