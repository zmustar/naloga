package si.kivi.naloga.ui.adapters
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import si.kivi.naloga.R
import si.kivi.naloga.data.model.Vozilo


class VoziloAdapter(private var vozila: List<Vozilo> = emptyList()):
    RecyclerView.Adapter<VoziloAdapter.VoziloViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VoziloViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return VoziloViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return vozila.size-1
    }

    fun setVozila(vozila2: List<Vozilo>){
        vozila = vozila2
    }

    override fun onBindViewHolder(holder: VoziloViewHolder, position: Int) {
        holder.bind(vozila.get(position))
    }

    inner class VoziloViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item, parent, false)) {
        private var mZnamka: TextView? = null
        private var mModel: TextView? = null


        init {
            mZnamka = itemView.findViewById(R.id.list_model)
            mModel = itemView.findViewById(R.id.list_znamka)
        }

        fun bind(vozilo: Vozilo) {
                mZnamka?.text = vozilo.znamka
                mModel?.text = vozilo.model


        }

    }
}