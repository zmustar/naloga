package si.kivi.naloga.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import si.kivi.naloga.R
import si.kivi.naloga.data.repository.getAvtomobili
import si.kivi.naloga.ui.ViewPages.AvtomobiliFragment
import si.kivi.naloga.ui.ViewPages.MotornaKolesaFragment
import si.kivi.naloga.ui.adapters.ViewPagerFragmentAdapter


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mPager = findViewById<ViewPager2>(R.id.pager)
        val pagerAdapter =  ViewPagerFragmentAdapter(getSupportFragmentManager(),getLifecycle())

        pagerAdapter.addFragment(AvtomobiliFragment.newInstance())
        pagerAdapter.addFragment(MotornaKolesaFragment.newInstance())

        mPager.adapter = pagerAdapter
        mPager.setCurrentItem(0)
        mPager.isUserInputEnabled = false
        mPager.invalidate()

        val mTabLayout = findViewById<TabLayout>(R.id.tabs)
        mTabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                mPager!!.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id: Int = item.getItemId()
        return if (id == R.id.scanqr) {
            val intent = Intent(this, ScanQRActivity::class.java)
            startActivity(intent)
            true
        } else super.onOptionsItemSelected(item)
    }

}
