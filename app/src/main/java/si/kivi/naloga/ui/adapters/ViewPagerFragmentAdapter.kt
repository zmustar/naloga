package si.kivi.naloga.ui.adapters

import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import si.kivi.naloga.ui.ViewPages.AvtomobiliFragment
import si.kivi.naloga.ui.ViewPages.MotornaKolesaFragment
import java.util.*


class ViewPagerFragmentAdapter(
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle
) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    private val arrayList =
        ArrayList<Fragment>()

    @Override
    fun getItem(position: Int): Fragment {
        return arrayList[position]
    }

    fun addFragment(fragment: Fragment) {
        arrayList.add(fragment)
    }
    @Override
    override fun getItemCount(): Int {
        return arrayList.size
    }
    @Override
    override fun createFragment(position: Int): Fragment {
        if(0<=position && position<arrayList.size){
            return arrayList.get(position)
        }
        return Fragment()
    }

}