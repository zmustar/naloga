package si.kivi.naloga.ui.ViewPages

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import si.kivi.naloga.R
import si.kivi.naloga.data.model.Vozilo
import si.kivi.naloga.data.repository.ResultListener
import si.kivi.naloga.data.repository.getAvtomobili
import si.kivi.naloga.data.repository.provideOkHttpClient
import si.kivi.naloga.ui.adapters.VoziloAdapter


class AvtomobiliFragment : Fragment(), ResultListener<Array<Vozilo>> {

    companion object {
        fun newInstance(): AvtomobiliFragment {
            return AvtomobiliFragment()
        }
    }
    lateinit var mRecyclerList: RecyclerView
    lateinit var mRefresh: SwipeRefreshLayout
    lateinit var mAdapter: VoziloAdapter


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_recyclerlist, container, false)
        mRecyclerList = view.findViewById(R.id.recyclerview)
        mRecyclerList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL ,false)
        mRefresh = view.findViewById<SwipeRefreshLayout>(R.id.refresh)
        mAdapter =  VoziloAdapter()
        mRecyclerList.adapter = mAdapter
        mRefresh.setOnRefreshListener {refreshList()}
        refreshList()
        return view
    }
    fun refreshList(){
        getAvtomobili(this);
    }
    fun updateList(vozila: Array<Vozilo>?){
        mAdapter.setVozila(vozila!!.asList())
        this.activity!!.runOnUiThread { mAdapter.notifyDataSetChanged() }
        mRefresh.isRefreshing = false
    }

    override fun onSuccess(result: Array<Vozilo>?) {
        updateList(result)
    }


}