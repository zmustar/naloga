package si.kivi.naloga.ui

import android.graphics.SurfaceTexture
import android.media.MediaPlayer
import android.os.Bundle
import android.view.TextureView.SurfaceTextureListener
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.Detector.Processor
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import kotlinx.coroutines.*
import si.kivi.naloga.R
import si.kivi.naloga.camera.AutoFitTextureView
import si.kivi.naloga.camera.PreviewFragment

interface PreviewListener{
    fun QR_SCANNED()
}

class ScanQRActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_q_r)


        val fragment = PreviewFragment.newInstance(this)
        fragment.setListenerQR(object: PreviewListener{
            override fun QR_SCANNED() {
                playSound()
            }

        })
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.fragmentContainer, fragment)
        fragmentTransaction.commit()


    }


    fun playSound(){
        val mediaPlayer: MediaPlayer = MediaPlayer.create(this, R.raw.zvok);
        runBlocking {
            mediaPlayer.isLooping = true
            mediaPlayer.start()
            val job = launch(Dispatchers.Default) {
                delay(5000)
                mediaPlayer.stop()
                finish()
            }
        }


    }

}