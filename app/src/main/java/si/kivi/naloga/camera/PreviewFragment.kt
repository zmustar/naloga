package si.kivi.naloga.camera

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageFormat
import android.graphics.SurfaceTexture
import android.hardware.camera2.*
import android.media.ImageReader
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import kotlinx.android.synthetic.main.activity_scan_q_r.*
import kotlinx.android.synthetic.main.fragment_preview.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import si.kivi.naloga.NalogaApp
import si.kivi.naloga.R
import si.kivi.naloga.ui.PreviewListener
import si.kivi.naloga.utils.Utils
import java.lang.Exception
import java.util.*

class PreviewFragment(val activity: Activity): Fragment() {

    companion object {
        const val REQUEST_CAMERA_PERMISSION = 233
        val TAG = PreviewFragment::class.qualifiedName

        fun newInstance(activity:Activity) = PreviewFragment(activity)



    }
    lateinit var listener: PreviewListener
    fun setListenerQR(listener2: PreviewListener){
        listener=listener2
    }

    private val MAX_PREVIEW_WIDTH = 1080
    private val MAX_PREVIEW_HEIGHT = 1960

    lateinit var barcodeDetector: BarcodeDetector
    private lateinit var captureSession: CameraCaptureSession
    private lateinit var captureRequestBuilder: CaptureRequest.Builder

    private val deviceStateCallback = object: CameraDevice.StateCallback(){
        override fun onOpened(p0: CameraDevice) {
            Log.d(TAG, "Camera device opened")
            if(p0!=null) {
                mCameraDevice = p0
                activity.runOnUiThread({
                    preview.setAspectRatio(Utils.getWidth(),Utils.getHeight())
                    preview.requestLayout()
                })
                val surfaceTexture = preview.surfaceTexture


                surfaceTexture.setDefaultBufferSize(MAX_PREVIEW_WIDTH,MAX_PREVIEW_HEIGHT)
                val surface= Surface(surfaceTexture)
                //PREVIEW
                captureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
                captureRequestBuilder.addTarget(surface)

                mCameraDevice.createCaptureSession(Arrays.asList(surface),
                    object: CameraCaptureSession.StateCallback(){
                        override fun onConfigureFailed(p0: CameraCaptureSession) {
                            Log.e(TAG,"Creating capture session failed")
                        }

                        override fun onConfigured(p0: CameraCaptureSession) {
                            if(p0!=null){
                                captureSession = p0
                                captureSession.setRepeatingRequest(captureRequestBuilder.build(), null,null)
                            }
                        }
                    }, null)

                // QR Scan
                lookforQR.start()
            }
        }

        override fun onDisconnected(p0: CameraDevice) {
            Log.d(TAG, "Camera device disconnected")
            mCameraDevice.close()
        }

        override fun onError(p0: CameraDevice, p1: Int) {
            Log.d(TAG, "Camera device error")
        }

    }
    val mainHandler = Handler(Looper.getMainLooper())

    private val lookforQR = object : Runnable {
        var mIsStopped = false;

        fun start(){
            mIsStopped=false
            this.run()
        }

        override fun run() {
            if(mIsStopped)
                return

            val preview: Bitmap = preview.bitmap
            val frameToProcess = Frame.Builder().setBitmap(preview).build()
            val barcodeResults = barcodeDetector.detect(frameToProcess)

            if (barcodeResults.size() > 0) {
                Log.d(TAG, "Barcode detected!")
                listener.QR_SCANNED()

            } else {
                Log.d(TAG, "No barcode found")
            }
            mainHandler.postDelayed(this, 1000)
        }

        fun stop() {
            mIsStopped=true
        }
    }

    private lateinit var mCameraDevice: CameraDevice
    private lateinit var backgroundThread: HandlerThread
    private lateinit var backgroundHandler: Handler

    private val mCameraManager by lazy {
        activity?.getSystemService(Context.CAMERA_SERVICE) as CameraManager
    }

    private fun closeCamera(){
        if(this::captureSession.isInitialized){
            captureSession.close()
        }
        if(this::mCameraDevice.isInitialized){
            mCameraDevice.close()
        }
    }

    private fun startBackgroundThread(){
        backgroundThread = HandlerThread("Camera2").also { it.start() }
        backgroundHandler = Handler(backgroundThread.looper)
    }

    private fun stopBackgroundThread(){

        backgroundThread.quitSafely()
        try{
            backgroundThread.join()
        } catch (e: InterruptedException){
            Log.e(TAG,e.toString())
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int,permissions: Array<out String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults)
    }

    @AfterPermissionGranted(REQUEST_CAMERA_PERMISSION)
    private fun checkCameraPermission(){
        if(EasyPermissions.hasPermissions(activity!!,Manifest.permission.CAMERA)){
            Log.d(TAG,"Permission for CAMERA GRANTED")
        }else{
            EasyPermissions.requestPermissions(activity!!,getString(R.string.kamera_pravice),REQUEST_CAMERA_PERMISSION,Manifest.permission.CAMERA)
        }
    }

    private fun <T> cameraCharacteristics(cameraId: String, key: CameraCharacteristics.Key<T>) :T{
        val characteristics = mCameraManager.getCameraCharacteristics(cameraId)
        return when(key){
            CameraCharacteristics.LENS_FACING -> characteristics.get(key)
            CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP -> characteristics.get(key)
            else -> throw IllegalArgumentException("Key not recognized")
        }
    }

    private fun cameraId(lens: Int):String{
        var deviceId = listOf<String>()
        try{
            val cameraIdList = mCameraManager.cameraIdList
            deviceId = cameraIdList.filter { lens == cameraCharacteristics(it, CameraCharacteristics.LENS_FACING) }
        }catch (e: Exception){
            Log.e(TAG,e.toString())
        }
        return deviceId[0]
    }


    val mSurfaceTextureListener: TextureView.SurfaceTextureListener = object :
        TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureAvailable(texture: SurfaceTexture, width: Int, height: Int) {
            openCamera()
        }

        override fun onSurfaceTextureSizeChanged(texture: SurfaceTexture,width: Int,height: Int) {
        }

        override fun onSurfaceTextureDestroyed(texture: SurfaceTexture): Boolean {
            return true
        }

        override fun onSurfaceTextureUpdated(texture: SurfaceTexture) {
        }
    }

    private fun openCamera(){
        checkCameraPermission()
        val deviceId = cameraId(CameraCharacteristics.LENS_FACING_BACK)
        try{
            mCameraManager.openCamera(deviceId, deviceStateCallback, backgroundHandler);
        } catch (e: Exception){
            Log.e(TAG,e.toString())
        }
    }

    override fun onResume() {
        super.onResume()
        barcodeDetector = BarcodeDetector.Builder(context!!)
            .setBarcodeFormats(Barcode.QR_CODE or Barcode.DATA_MATRIX)
            .build()
        startBackgroundThread()
        if(preview.isAvailable)
            openCamera()
        else
            preview.surfaceTextureListener = mSurfaceTextureListener
    }
    override fun onPause(){
        lookforQR.stop()
        closeCamera()
        stopBackgroundThread()
        super.onPause()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_preview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
    }

}