package si.kivi.naloga.data.model

import com.google.gson.annotations.SerializedName


class Vozilo(@SerializedName("znamka") val znamka: String,
             @SerializedName("model") val model: String) {
}