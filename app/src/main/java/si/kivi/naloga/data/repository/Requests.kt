package si.kivi.naloga.data.repository


import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import dagger.Provides
import okhttp3.*
import org.koin.java.KoinJavaComponent.get
import si.kivi.naloga.data.model.Vozilo
import si.kivi.naloga.utils.BackendMocker
import java.io.IOException
import java.lang.Exception
import javax.inject.Named
import javax.inject.Singleton

val TAG= "REQUESTS"
@Provides
@Singleton
@Named("OkHttpClient")
fun provideOkHttpClient(
    @Named("BackendMocker") authInterceptor: Interceptor
): OkHttpClient = OkHttpClient
    .Builder()
    .addInterceptor(authInterceptor)
    .build()
@Provides
@Singleton
@Named("BackendMocker")
fun provideMockInterceptor(): Interceptor = BackendMocker()

const val BASEURL = "https://www.whateverURL.com"

//GET Avtomobili
fun getAvtomobili(listener: ResultListener<Array<Vozilo>>){
    val client = provideOkHttpClient(provideMockInterceptor())
    val request: Request = Request.Builder()
        .url(BASEURL+"/avtomobili")
        .get()
        .build()

    client.newCall(request).enqueue(object : Callback {
        override fun onFailure(call: Call?, e: IOException) {
            e.printStackTrace()
        }

        @Throws(IOException::class)
        override fun onResponse(call: Call?, response: Response) {
            if (!response.isSuccessful()) throw IOException("Unexpected code $response")
            try {
            val jsonObject: JsonObject = JsonParser.parseString(response.body()?.string()).getAsJsonObject()
            val avtomobili = jsonObject.getAsJsonArray("avtomobili")
            val array: Array<Vozilo> = Gson().fromJson(avtomobili,Array<Vozilo>::class.java)
            listener.onSuccess(array);
            } catch (e: Exception){
                Log.e(TAG,e.toString())
            }
        }
    })
}
//GET MotornaKolesa
fun getMotornaKolesa(listener: ResultListener<Array<Vozilo>>){
    val client = provideOkHttpClient(provideMockInterceptor())
    val request: Request = Request.Builder()
        .url(BASEURL+"/motornakolesa")
        .get()
        .build()

    client.newCall(request).enqueue(object : Callback {
        override fun onFailure(call: Call?, e: IOException) {
            e.printStackTrace()
        }

        @Throws(IOException::class)
        override fun onResponse(call: Call?, response: Response) {
            if (!response.isSuccessful()) throw IOException("Unexpected code $response")
            try {
                val jsonObject: JsonObject = JsonParser.parseString(response.body()?.string()).getAsJsonObject()
                val avtomobili = jsonObject.getAsJsonArray("motorji")
                val array: Array<Vozilo> = Gson().fromJson(avtomobili,Array<Vozilo>::class.java)
                listener.onSuccess(array);
            } catch (e: Exception){
                Log.e(TAG,e.toString())
            }

        }
    })
}


//ResultListener
interface ResultListener<T> {
    fun onSuccess(result: T?)
}
